/**
 * @module Actions/Jobs
 * @desc Job Posts Actions
 */
import { createAction } from 'utils/helpers';

import { ActionTypes } from '../literals';

export const getJobs = createAction(ActionTypes.GET_JOB_POSTS_REQUEST, query => ({
  query,
}));
