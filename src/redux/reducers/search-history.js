import { createReducer } from 'utils/helpers';
import { ActionTypes, STATUS } from '../literals';

export const searchHistoryState = {
  status: STATUS.IDLE,
  data: [],
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  searchHistory: createReducer(
    {
      [ActionTypes.GET_SEARCH_HISTORY_REQUEST]: draft => {
        draft.status = STATUS.RUNNING;
      },
      [ActionTypes.GET_SEARCH_HISTORY_SUCCESS]: (draft, payload) => {
        draft.status = STATUS.READY;
        draft.data = payload;
      },
      [ActionTypes.GET_SEARCH_HISTORY_FAILURE]: draft => {
        draft.status = STATUS.ERROR;
      },
    },
    searchHistoryState
  ),
};
