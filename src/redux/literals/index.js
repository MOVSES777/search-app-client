import { keyMirror } from 'utils/helpers';

export const ActionTypes = keyMirror({
  GET_JOB_POSTS_REQUEST: undefined,
  GET_JOB_POSTS_SUCCESS: undefined,
  GET_JOB_POSTS_FAILURE: undefined,
  GET_AUTO_COMPLETE_REQUEST: undefined,
  GET_AUTO_COMPLETE_SUCCESS: undefined,
  GET_AUTO_COMPLETE_FAILURE: undefined,
  GET_SEARCH_HISTORY_REQUEST: undefined,
  GET_SEARCH_HISTORY_SUCCESS: undefined,
  GET_SEARCH_HISTORY_FAILURE: undefined,
});

export const STATUS = {
  IDLE: 'idle',
  RUNNING: 'running',
  READY: 'ready',
  SUCCESS: 'success',
  ERROR: 'error',
};
