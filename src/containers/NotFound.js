import React from 'react';
import { Link } from 'react-router-dom';

function NotFound() {
  return (
    <div key='404'>
      <p>Not Found</p>
      <Link to='/'>go home</Link>
    </div>
  );
}

export default NotFound;
