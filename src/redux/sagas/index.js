import { all, fork } from 'redux-saga/effects';

import jobs from './jobs';
import autoComplete from './auto-complete';
import searchHistory from './search-history';

/**
 * rootSaga
 */
export default function* root() {
  yield all([fork(jobs), fork(autoComplete), fork(searchHistory)]);
}
