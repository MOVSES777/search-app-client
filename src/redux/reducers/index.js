import { combineReducers } from 'redux';
import jobs from './jobs';
import autoComplete from './auto-complete';
import searchHistory from './search-history';

const reducers = combineReducers({
  ...jobs,
  ...autoComplete,
  ...searchHistory,
});

export default reducers;
