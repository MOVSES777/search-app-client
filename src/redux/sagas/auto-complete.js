import { all, call, put, takeEvery } from 'redux-saga/effects';
import { ActionTypes } from 'redux/literals';
import { fetchAutoCompleteApi } from 'redux/api/auto-complete';

export function* fetchAutoComplete({ payload }) {
  const response = yield call(fetchAutoCompleteApi, payload.query);
  const data = yield response.data;
  yield put({ type: ActionTypes.GET_AUTO_COMPLETE_SUCCESS, data });
}

export function* loadAutoComplete() {
  yield takeEvery(ActionTypes.GET_AUTO_COMPLETE_REQUEST, fetchAutoComplete);
}

export default function* rootSaga() {
  yield all([loadAutoComplete()]);
}
