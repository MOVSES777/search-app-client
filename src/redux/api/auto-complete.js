import client from './client';

export const fetchAutoCompleteApi = params => client().get('/autocomplete', { params });
