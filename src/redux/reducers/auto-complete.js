import { createReducer } from 'utils/helpers';
import { ActionTypes, STATUS } from '../literals';

export const jobsState = {
  status: STATUS.IDLE,
  data: [],
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  autoComplete: createReducer(
    {
      [ActionTypes.GET_AUTO_COMPLETE_REQUEST]: draft => {
        draft.status = STATUS.RUNNING;
      },
      [ActionTypes.GET_AUTO_COMPLETE_SUCCESS]: (draft, payload) => {
        draft.status = STATUS.READY;
        draft.data = payload;
      },
      [ActionTypes.GET_AUTO_COMPLETE_FAILURE]: draft => {
        draft.status = STATUS.ERROR;
      },
    },
    jobsState
  ),
};
