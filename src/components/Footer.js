import React from 'react';
import { Layout } from 'antd';

const { Footer: FooterLayout } = Layout;

const Footer = () => {
  return <FooterLayout style={{ textAlign: 'center' }}>Search App</FooterLayout>;
};

export default React.memo(Footer);
