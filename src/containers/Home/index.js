import React from 'react';
import { withRouter } from 'react-router-dom';
import { Typography } from 'antd';
import SearchList from './partials/SearchList';
import Search from './partials/Search';

const { Title } = Typography;

const Home = ({ match }) => {
  const keyword = match.params?.keyword || 'react';

  return (
    <>
      <Title style={{ textAlign: 'center' }} level={2}>
        Github Jobs
      </Title>

      <Search keyword={keyword} />
      <SearchList keyword={keyword} />
    </>
  );
};

export default withRouter(React.memo(Home));
