import React, { useState } from 'react';
import { AutoComplete, notification } from 'antd';
import { withRouter } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { getAutoComplete } from 'redux/actions';
import { usePrevious, useUpdateEffect } from 'react-use';
import { useShallowEqualSelector } from 'utils/hooks';
import { STATUS } from 'redux/literals';

const Search = ({ keyword, history }) => {
  const dispatch = useDispatch();
  const [options, setOptions] = useState([]);
  const [text, setText] = useState(keyword);

  const { data, message, status } = useShallowEqualSelector(({ autoComplete }) => ({
    data: autoComplete.data?.data || [],
    message: autoComplete?.message || '',
    status: autoComplete?.status || STATUS.IDLE,
  }));
  const previousStatus = usePrevious(status);

  useUpdateEffect(() => {
    if (previousStatus !== status && status === STATUS.ERROR) {
      dispatch(notification.warning(message));
    }
  }, [message, previousStatus, status]);

  useUpdateEffect(() => {
    if (data.length) {
      const result = [
        { value: text },
        ...data.map(item => ({
          value: item.suggestion,
        })),
      ];

      setOptions(result);
    }
  }, [data]);

  const onSearch = searchText => {
    setText(searchText);
    if (searchText.length >= 1) {
      dispatch(getAutoComplete({ keyword: searchText }));
    }
  };

  const onSelect = searchText => {
    history.push(`/search/${searchText}`);
  };

  const loading = status === STATUS.RUNNING;

  return (
    <AutoComplete
      options={options}
      style={{ width: '100%' }}
      onSelect={onSelect}
      onSearch={onSearch}
      loading={loading}
      defaultValue={keyword}
      placeholder='Keyword'
    />
  );
};

export default withRouter(React.memo(Search));
