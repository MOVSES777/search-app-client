import React, { useEffect, useState } from 'react';
import 'antd/dist/antd.css';
import { List, Avatar, notification } from 'antd';
import parse from 'html-react-parser';
import { useDispatch } from 'react-redux';
import { getJobs } from 'redux/actions';
import { usePrevious, useUpdateEffect } from 'react-use';
import { useShallowEqualSelector } from 'utils/hooks';
import { STATUS } from 'redux/literals';

const SearchList = ({ keyword }) => {
  const dispatch = useDispatch();
  const [listData, setListData] = useState([]);
  const { data, message, status } = useShallowEqualSelector(({ jobs }) => ({
    data: jobs.data?.data || [],
    message: jobs?.message || '',
    status: jobs?.status || STATUS.IDLE,
  }));
  const previousStatus = usePrevious(status);

  useEffect(() => {
    dispatch(getJobs({ keyword }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [keyword]);

  useUpdateEffect(() => {
    if (previousStatus !== status && status === STATUS.ERROR) {
      dispatch(notification.warning(message));
    }
  }, [message, previousStatus, status]);

  useUpdateEffect(() => {
    if (data.length) {
      const items = [];

      data.forEach(item => {
        items.push({
          href: item.url,
          title: item.title,
          avatar: item.company_logo,
          description: item.company,
          content: `${item.description.replace(/(<([^>]+)>)/gi, '').slice(0, 400)}...`,
        });
      });

      setListData(items);
    }
  }, [data]);

  const loading = status === STATUS.RUNNING;

  return (
    <List
      itemLayout='vertical'
      size='large'
      loading={loading}
      pagination={{
        pageSize: 20,
      }}
      dataSource={listData}
      renderItem={item => (
        <List.Item
          key={item.id}
          actions={[
            <a href={item.href} target='_blank' rel='noreferrer'>
              Apply
            </a>,
          ]}
        >
          <List.Item.Meta
            avatar={<Avatar src={item.avatar} />}
            title={<a href={item.href}>{item.title}</a>}
            description={parse(item.description)}
          />
          {item.content}
        </List.Item>
      )}
    />
  );
};

export default React.memo(SearchList);
