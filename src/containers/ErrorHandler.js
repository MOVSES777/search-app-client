import React from 'react';

export default class ErrorHandler extends React.Component {
  state = {
    error: null,
  };

  componentDidCatch(error, info) {
    const { onError } = this.props;

    /* istanbul ignore else */
    if (typeof onError === 'function') {
      try {
        onError.call(this, error, info?.componentStack);
      } catch {
        // ignore
      }
    }

    this.setState({ error });
  }

  render() {
    const { children } = this.props;
    const { error } = this.state;

    if (error === null) {
      return children;
    }

    const message = error.toString();

    return <div>{message}</div>;
  }
}
