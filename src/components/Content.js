import styled from 'styled-components';
import { Layout } from 'antd';

const { Content } = Layout;

const StyledContent = styled(Content)`
  //width: 1040px;
`;

/** @component */
export default StyledContent;
