import client from './client';

export const fetchJobsApi = params => client().get('/search', { params });
