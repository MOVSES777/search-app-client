import { applyMiddleware, compose, createStore } from 'redux';
import reducers from 'redux/reducers';
import rootSaga from 'redux/sagas';
import middleware, { sagaMiddleware } from './middleware';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/* istanbul ignore next */
export const configStore = (initialState = {}, additionalMiddleware = []) => {
  const store = createStore(
    reducers,
    initialState,
    composeEnhancer(applyMiddleware(...additionalMiddleware, ...middleware))
  );

  sagaMiddleware.run(rootSaga);

  if (module.hot) {
    module.hot.accept('redux/reducers', () => {
      store.replaceReducer(reducers);
    });
  }

  return store;
};
