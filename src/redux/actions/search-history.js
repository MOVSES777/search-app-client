/**
 * @module Actions/Jobs
 * @desc Job Posts Actions
 */
import { createAction } from 'utils/helpers';

import { ActionTypes } from '../literals';

export const getSearchHistory = createAction(ActionTypes.GET_SEARCH_HISTORY_REQUEST, query => ({
  query,
}));
