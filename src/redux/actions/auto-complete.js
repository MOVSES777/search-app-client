/**
 * @module Actions/Jobs
 * @desc Job Posts Actions
 */
import { createAction } from 'utils/helpers';

import { ActionTypes } from '../literals';

export const getAutoComplete = createAction(ActionTypes.GET_AUTO_COMPLETE_REQUEST, query => ({
  query,
}));
