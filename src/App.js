import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import { Layout } from 'antd';
import { Content, Header, Footer } from 'components';
import 'antd/dist/antd.css';
import './App.css';
import NotFound from 'containers/NotFound';
import Home from 'containers/Home';
import History from 'containers/History';

function App() {
  return (
    <BrowserRouter>
      <Layout className='layout'>
        <Header />
        <Content
          style={{
            padding: 24,
            margin: 0,
            minHeight: 280,
          }}
        >
          <div className='site-layout-content'>
            <Switch>
              <Route path='/search/:keyword?' exact component={Home} />
              <Route path='/history' exact component={History} />
              <Route path='/' exact component={Home} />
              <Route component={NotFound} />
            </Switch>
          </div>
        </Content>
        <Footer />
      </Layout>
    </BrowserRouter>
  );
}

export default App;
