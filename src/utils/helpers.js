import produce from 'immer';
import is from 'is-lite';

/**
 * Create an action
 */
export function createAction(type, payloadCreator) {
  if (!payloadCreator) {
    throw new TypeError('Expected a function');
  }

  return (...args) => ({
    type,
    payload: payloadCreator(...args),
  });
}

/**
 * Create a reducer
 */
export function createReducer(actionsMap, defaultState) {
  return (state = defaultState, action) =>
    produce(state, draft => {
      const fn = actionsMap[action.type];
      if (fn) {
        return fn(draft, action);
      }

      return draft;
    });
}

export function keyMirror(input) {
  if (!is.plainObject(input)) {
    throw new TypeError('Expected an object');
  }

  const output = {};

  for (const key in input) {
    if (!Object.prototype.hasOwnProperty.call(output, key)) {
      output[key] = key;
    }
  }

  return output;
}
