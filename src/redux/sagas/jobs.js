import { all, call, put, takeEvery } from 'redux-saga/effects';
import { ActionTypes } from 'redux/literals';
import { fetchJobsApi } from 'redux/api/jobs';

export function* fetchJobs({ payload }) {
  const response = yield call(fetchJobsApi, payload.query);
  const data = yield response.data;
  yield put({ type: ActionTypes.GET_JOB_POSTS_SUCCESS, data });
}

export function* loadJobs() {
  yield takeEvery(ActionTypes.GET_JOB_POSTS_REQUEST, fetchJobs);
}

export default function* rootSaga() {
  yield all([loadJobs()]);
}
