import client from './client';

export const fetchSearchHistoryApi = params => client().get('/search-history', { params });
