import React from 'react';
import { Layout, Menu } from 'antd';
import { withRouter } from 'react-router-dom';

const { Header: HeaderLayout } = Layout;

const Header = ({ history }) => {
  return (
    <HeaderLayout>
      <div className='logo' />
      <Menu theme='dark' mode='horizontal'>
        <Menu.Item key='1' onClick={() => history.push({ pathname: '/' })}>
          Home
        </Menu.Item>
        <Menu.Item key='2' onClick={() => history.push({ pathname: '/history' })}>
          History
        </Menu.Item>
      </Menu>
    </HeaderLayout>
  );
};

export default withRouter(React.memo(Header));
