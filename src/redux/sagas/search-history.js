import { all, call, put, takeEvery } from 'redux-saga/effects';
import { ActionTypes } from 'redux/literals';
import { fetchSearchHistoryApi } from 'redux/api/search-history';

export function* fetchSearchHistory({ payload }) {
  const response = yield call(fetchSearchHistoryApi, payload.query);
  const data = yield response.data;
  yield put({ type: ActionTypes.GET_SEARCH_HISTORY_SUCCESS, data });
}

export function* loadSearchHistory() {
  yield takeEvery(ActionTypes.GET_SEARCH_HISTORY_REQUEST, fetchSearchHistory);
}

export default function* rootSaga() {
  yield all([loadSearchHistory()]);
}
