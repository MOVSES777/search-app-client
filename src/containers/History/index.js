import React, { useEffect, useState } from 'react';
// import { usePrevious, useMount, useUpdateEffect } from 'react-use';
import { getSearchHistory } from 'redux/actions';
import { useDispatch } from 'react-redux';
import { usePrevious, useUpdateEffect } from 'react-use';
import { notification, Table, Typography } from 'antd';
import { useShallowEqualSelector } from 'utils/hooks';
import { STATUS } from 'redux/literals';

const { Title } = Typography;

const PAGE_SIZE = 10;

const History = () => {
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);

  const { data, message, status } = useShallowEqualSelector(({ searchHistory }) => ({
    data: searchHistory.data?.data || [],
    message: searchHistory?.message || '',
    status: searchHistory?.status || STATUS.IDLE,
  }));

  useEffect(() => {
    dispatch(getSearchHistory({ $skip: (page - 1) * PAGE_SIZE, '$sort[updatedAt]': -1 }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const previousStatus = usePrevious(status);
  useUpdateEffect(() => {
    if (previousStatus !== status && status === STATUS.ERROR) {
      dispatch(notification.warning(message));
    }
  }, [message, previousStatus, status]);

  const columns = [
    {
      title: 'Keyword',
      dataIndex: 'keyword',
      key: 'keyword',
    },
    {
      title: 'Date',
      dataIndex: 'createdAt',
      key: 'createdAt',
    },
  ];

  return (
    <div>
      <Title style={{ textAlign: 'center' }} level={2}>
        Search History
      </Title>

      <Table
        columns={columns}
        dataSource={data.data || []}
        rowKey='id'
        pagination={{
          position: 'bottom',
          current: page,
          total: data.total,
          pageSize: PAGE_SIZE,
          onChange: current => setPage(current),
        }}
      />
    </div>
  );
};

export default React.memo(History);
