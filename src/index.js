import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { configStore } from 'redux/store';
import ErrorHandler from 'containers/ErrorHandler';
import App from './App';

const store = configStore();

window.store = store;

render(
  <Provider store={store}>
    <ErrorHandler>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ErrorHandler>
  </Provider>,
  document.getElementById('root')
);
