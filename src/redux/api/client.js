import axios from 'axios';
import { API_ROOT } from 'config/env';

// eslint-disable-next-line import/no-anonymous-default-export
export default (headers = {}) => {
  return axios.create({
    baseURL: API_ROOT,
    headers: {
      ...headers,
    },
  });
};
